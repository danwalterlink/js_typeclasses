// nodejs per 10.3.0 needs --experimental-modules flag

/*
  Syntax: (typeSet) :: (type)
  inheritance chain:
    object | string | number | symbol | bool | undefined | null
    
    every typed token has the subtype of itself; meaning it implements itself as interface. this can be driven further as implementation as long as the designation of types is inferrable by the compiler.
*/

const typeset = token =>
  { baseProtoype = getPrototype(token